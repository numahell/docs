# Synchronisation avec KDE PIM

Il est possible de synchroniser votre agenda et votre carnet d'adresse avec KOrganizer et KAddressbook. Pour cela, vous devez&nbsp;:

  1. ouvrir KOrganizer et faire un clic droit dans la zone des calendriers (en bas, à gauche) et choisir **Ajouter un calendrier**

  ![image ajout calendrier kde](../images/kde-ajout-calendrier.png)
  * dans la liste de la fenêtre suivante, tapez **dav** pour sélectionner **Ressource pour logiciel de groupe DAV**

  ![image kde dav](../images/kde-dav.png)
  * entrer votre identifiant et mot de passe Framagenda
  * sélectionner **ownCloud** dans la liste des serveurs de logiciel

  ![image sélection serveur de logiciel](../images/kde-serveur-logiciel.png)
  * entrer `framagenda.org` (sans le `https`) dans **Hôte**

  ![image configuration de l'hôte kde](../images/kde-hote.png)
  * cliquer sur **Tester une connexion** (cela peut prendre du temps)
  * choisir un nom d'affichage (comme **Framagenda** par exemple) et cliquer sur **OK**

  ![image configuration générale kde](../images/kde-conf-generale.png)
