# Framasphère

Framasphère est un nœud (appelé pod) du réseau social libre Diaspora*.
Ses ~[35 000](https://framasphere.org/statistics) utilisateurs peuvent communiquer avec les ~[600 000](http://the-federation.info/) que compte l’ensemble du réseau.

## Pourquoi utiliser Framasphère ?

Depuis Framasphère, vous pouvez :

- échanger des messages et photos avec n'importe quelle autre personne du réseau Diaspora*
- gérer vos contacts, tags, mentions, repartages…
- garder le contrôle de vos données
- publier sur d'autres réseaux sociaux (Twitter, Tumblr ou Wordpress)
- en somme, apprécier toutes les fonctionnalités d'un réseau social sans craindre la censure et dans le respect de votre vie privée


## Table des matières

* [le guide du parfait débutant](https://fr.wikibooks.org/wiki/Diaspora_:_Le_guide_du_parfait_débutant)
* [démarrer sur diaspora*](https://diasporafoundation.org/getting_started/sign_up)
* [section aide de Framasphère](https://framasphere.org/help)
* [les outils utiles](https://wiki.diasporafoundation.org/Tools_to_use_with_Diaspora) (en anglais)
* [Kit de base](kit-de-base.html)


## Démarrer sur diaspora*

* [1 - Connexion](1-connexion.html)
* [2 - Interface](2-interface.html)
* [2b - Interface mobile](2b-interface-mobile.html)
* [3 - Aspects](3-aspects.html)
* [4 - Échange](4-echange.html)
* [5 - Partage](5-partage.html)
* [6 - Notification et conversation](6-notification-conversation.html)
* [7 - Pour terminer](7-fin.html)

## Pour aller plus loin&nbsp;:

  * Application Android&nbsp;:
    * [dandelion*](https://f-droid.org/packages/com.github.dfa.diaspora_android/)
  * [Utiliser Framasphère](https://framasphere.org)
  * Un service proposé dans le cadre de la campagne [Dégooglisons Internet](https://degooglisons-internet.org)
