# Fonctionnalités

## Raccourcis

Vous pouvez voir les raccourcis pour Framindmap avec compte sur la page : https://framindmap.org/c/keyboard

## Publier une carte

Pour publier une carte mentale vous devez :

* cliquer sur l'icône **Publier** : ![icône Publier](images/mindmap_publier.png)
* cocher **Activer le partage**

Si vous souhaitez *embarquer* la carte sur un site, vous devez alors copier le code du style :

```
<iframe style="width:600px;height:400px;border: 1px
solid black" src="https://framindmap.org/c/maps/CHIFFRES/embed?zoom=1"> </iframe>
```
Sinon, vous pouvez cliquer sur l'onglet **URL Publique** et copier l'url de votre carte pour la donner aux personnes souhaitées.

N'oubliez pas de cliquer sur **Accepter** pour confirmer la publication de votre carte.

## Enregistrer une carte sur son ordinateur

### Avec Wisemapping (nécessitant un compte)

Vous devez :
  * cliquer sur l'icône **Exporter**  
  ![icone Exporter mindmap](images/wisemapping_sauvegarde.png)
  * sélectionner le format souhaité
  * cliquer sur **Accepter**

### Avec Mindmaps (sans compte)

Vous devez :

  * cliquer sur **Carte mentale**
  * cliquer sur **Enregistrer**

  ![mindmap sauvegarde](images/mindmap_sauvegarde.png)
    * si vous souhaitez enregistrer sur votre **navigateur**, vous devez cliquer sur **Enregistrer** dans la partie **Stockage local** (attention, vous n'aurez plus la carte si vous changez de navigatuer ou si vous videz le cache)
    * si vous souhaitez l’enregistrer sur votre **ordinateur** vous devez cliquer sur **Enregistrer** dans la partie **Dans un fichier**

## Retour à la ligne dans un nœud

### Avec Wisemapping (nécessitant un compte)

Vous devez faire `ctrl`+`Entrée` pour aller à la ligne (`Entrée` simplement enregistre le nœud).

### Avec Mindmaps (sans compte)

Vous devez faire `maj`+`Entrée` pour aller à la ligne (`Entrée` simplement enregistre le nœud).
