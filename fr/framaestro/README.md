# Framaestro

Avec [Framaestro](https://framaestro.org), c’est à vous d’orchestrer un
bureau collaboratif comme vous l’entendez&nbsp;! Il vous suffit de [créer votre projet](https://framaestro.org)
, d’ajouter les cadres que vous voulez y voir , de les arranger comme
bon vous semble, puis de partager l’URL (l’adresse web)
de votre Framaestro avec vos collaborateurs et collaboratrices. Dès
lors, vous pouvez utiliser ensemble et en même temps, tous les outils

Sur une seule et même page web, à l’intérieur de cadres, vous pouvez afficher&nbsp;:

-   un Framapad (pour écrire collaborativement) qui peut durer un jour,
    une semaine, un, deux ou six mois, ou même un an&nbsp;;
-   un Framacalc, afin d’avoir un tableur sous la main&nbsp;;
-   un Framémo, pour organiser ses petites notes dans des colonnes&nbsp;;
-   un Framavectoriel, pour dessiner ce que vous voulez&nbsp;;
-   une visio-conférence via Framatalk&nbsp;;
-   un petit tchat pour discuter [en
    IRC](https://fr.wikipedia.org/wiki/Internet_Relay_Chat)&nbsp;;
-   et surtout la (ou les) page(s) web de votre choix (pour travailler
    sur un site web, par exemple)…


Voici une capture d’une session Framaestro.
![](https://framablog.org/wp-content/uploads/2017/01/slide-framaestro01-1024x350.jpg)


Tutoriel pour bien débuter&nbsp;: [Framastro, exemple d’utilisation](exemple-d-utilisation.html)

### Pour aller plus loin&nbsp;:

-   Utiliser [Framaestro](https://framaestro.org)
-   [Installer
    Framaestro](http://framacloud.org/cultiver-son-jardin/installation-de-framaestro/)
    sur votre serveur (tuto)
-   Un service proposé dans le cadre de la campagne [Dégooglisons
    Internet](https://degooglisons-internet.org)
