Utiliser l'authentification en deux étapes
===============================

L'authentification en deux étapes (parfois abrégée 2FA pour « Two-factor authentication ») permet de protéger votre compte Nextcloud contre les accès non-autorisés. Pour cela elle exige deux « preuves » distinctes de votre identité, par exemple une information *connue* (comme un mot de passe) et quelque chose qu'on *possède* (comme une clé physique). Habituellement, le premier facteur d'authentification correspond à un mot de passe qu'on connaît déjà et le second peut être un SMS qu'on reçoit ou un code qu'on génère sur un téléphone ou un appareil (quelque chose qu'on *possède*). Nextcloud prend en charge différentes méthodes pour la double authentification et il est possible d'en ajouter.


Ce n'est que lorsqu'une application d'authentification en deux étapes a été activée par votre administrateur que vous pourrez l'activer et la configurer dans [les préférences utilisateur](userpreferences.md) (voir ci-dessous)


Configurer l'authentification à deux facteurs
-------------------------------------


Dans les paramètres personnels, recherchez le paramètre « *Second-factor Auth* ». Dans cet exemple, il s'agit de TOTP, un code temporel compatible avec Google Authenticator.


![](images/totp_enable.png)


Vous pouvez voir votre secret et un code QR qui peut être scanné depuis l'application TOTP de votre téléphone (ou d'un autre appareil). Selon l'application ou l'outil utilisé, saisissez le code ou scannez le code QR et votre appareil affichera un code de connexion qui changera toutes les 30 secondes.


Codes de récupération en cas de perte de votre deuxième facteur d'authentification
-----------------------------------------------

Vous devriez toujours créer des codes de sauvegarde pour le 2FA. Si votre deuxième facteur d'authentification vous est volé ou ne fonctionne pas, vous pourrez tout de même utiliser un de ces codes pour débloquer l'accès à votre compte. Cela fonctionne en pratique comme une sauvegarde du second facteur. Pour obtenir vos codes de récupération, allez dans vos Informations personnelles puis dans la section *Codes de récupération pour l'authentification en deux étapes*. Choisissez *Générer des codes de récupération*.

![](images/2fa_backupcode_1.png)

Vous verrez alors apparaître une liste des codes de récupération à usage unique.

![](images/2fa_backupcode_2.png)

Vous devez conserver ces codes en lieu sûr, là où vous saurez les retrouver. Ne les rangez pas avec votre deuxième facteur, comme votre téléphone mobile, mais assurez-vous qu'en cas de perte de l'un, vous avez encore accès à l'autre. Les conserver chez soi est probablement la meilleure chose à faire.

S'identifier avec une authentification en deux étapes
-----------------------------------------

Après vous être déconnecté si vous devez vous connecter à nouveau, vous verrez une fenêtre vous demandant de saisir le code TOTP dans votre navigateur. Il vous suffit de saisir votre code :

![](images/totp_login_2.png)

Si le code est correct, vous serez redirigé vers votre compte Nextcloud.

> **note**
>
> Comme le code est indexé sur le temps, il est important que les horloges de vos serveurs et de votre téléphones mobile soient pratiquement synchrones. Un décalage de quelques secondes ne posera pas de problème.


Utiliser une application client avec l'authentification en deux étapes
--------------------------------------------------------

Une fois qui vous avez activé l'authentification en deux étapes, les clients ne seront plus capables de se connecter avec votre mot de passe à moins qu'ils supportent aussi la double authentification. Pour y suppléer, vous devez générer des mots de passe spécifiques pour chaque appareil. Voir [Gérer les appareils et les navigateurs connectés](session_management.md) pour plus d'information sur la démarche à suivre.
